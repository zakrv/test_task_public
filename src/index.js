import React from 'react';
import ReactDOM from 'react-dom';
import CarFiled from './components/CarFiled/index.tsx';
import { fetchData } from './api/fetchData';
import './styles/page.css';

ReactDOM.render(React.createElement(CarFiled, { fetchDataMethod: fetchData }), document.getElementById('root'));
