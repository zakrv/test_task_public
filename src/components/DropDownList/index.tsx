import React, { Fragment } from 'react';
import { Dimensions } from '../../interfaces';

import './style.css';

interface Props {
    data: Array<string>;
    onMouseEnter: (dimensions: Dimensions, item: string) => void;
    onMouseLeave: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    selected: string;
}

const DropDownList: React.FC<Props> = ({ data, onMouseEnter, onMouseLeave, selected }) => {
    if (data.length === 0) return null;

    const handleMouseEnter = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, item: string) => {
        const dimensions = event.currentTarget.getBoundingClientRect();
        onMouseEnter(dimensions, item);
    };

    const renderListItem = (item: string, i: number) => (
        <div
            key={`${i}_DropDownListItem`}
            className={`DropDownListItem ${item === selected ? 'selected' : ''}`}
            onMouseEnter={event => handleMouseEnter(event, item)}
            onMouseLeave={onMouseLeave}
        >
            {item}
        </div>
    );

    return (
        <Fragment>
            <div className="DropDownListDivider"></div>
            <div className="DropDownList">{data.map(renderListItem)}</div>
        </Fragment>
    );
};

export default DropDownList;
