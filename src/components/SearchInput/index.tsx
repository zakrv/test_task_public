import React, { useRef, useEffect, useLayoutEffect } from 'react';

import './styles.css';

interface Props {
    onChange: (value: string) => void;
    inputValue: string;
    autoFocus?: boolean;
    escapeClearValue?: boolean;
    children: React.ReactNode;
}

const SearchInput: React.FC<Props> = ({
    onChange,
    inputValue,
    autoFocus = false,
    escapeClearValue = true,
    children,
}) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {
            target: { value },
        } = event;
        onChange(value);
    };

    const inputRef = useRef<HTMLInputElement | null>(null);

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.keyCode === 27) {
            onChange('');
        }
    };

    useLayoutEffect(() => {
        if (autoFocus) {
            inputRef.current && inputRef.current.focus();
        }
    }, []);

    useEffect(() => {
        if (escapeClearValue) {
            document.addEventListener('keydown', handleKeyDown);
            return () => {
                document.removeEventListener('keydown', handleKeyDown);
            };
        }
    }, []);

    return (
        <div className="SearchInput">
            <input
                ref={inputRef}
                onChange={handleChange}
                value={inputValue}
                placeholder="Enter Vehicle Make"
                spellCheck={false}
            />
            {children && children}
        </div>
    );
};

export default SearchInput;
