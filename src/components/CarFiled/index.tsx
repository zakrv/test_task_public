import React, { useState, useEffect } from 'react';
import { isEmpty } from 'lodash';
import { Dimensions } from '../../interfaces';
import SearchInput from '../SearchInput';
import DropDownList from '../DropDownList';
import PopUp from '../PopUp';

import './styles.css';

const getDataByQuery = (data: Array<string>, query: string) => {
    return data.filter(item => item.toLowerCase().startsWith(query.toLowerCase()));
};

interface FetchedData {
    [key: string]: Array<string>;
}

interface Props {
    fetchDataMethod: () => Promise<FetchedData>;
}

const CarFiled: React.FC<Props> = ({ fetchDataMethod }) => {
    // fetched data
    const [data, setData] = useState<FetchedData>({});
    const [companies, setCompanies] = useState<Array<string>>([]);
    const [models, setModels] = useState<Array<string>>([]);

    const [query, setQuery] = useState<string>('');

    const [selectedCompany, setSelectedCompany] = useState<string>('');
    const [selectedModel, setSelectedModel] = useState<string>('');

    const [popUpDimensions, setPopUpDimensions] = useState<Dimensions>({ y: 0, height: 0 });

    const handleMouseEnter = (dimensions: Dimensions, value: string) => {
        setPopUpDimensions(dimensions);
        setSelectedCompany(value);
        setModels(data[value]);
    };

    const handleMouseLeave = (event: any) => {
        if (
            event.relatedTarget &&
            event.relatedTarget.classList &&
            !['PopUpWrapper', 'PopUp', 'CarModel'].includes(event.relatedTarget.classList.value)
        ) {
            resetState();
        }
    };

    const resetState = () => {
        setSelectedCompany('');
        setSelectedModel('');
        setPopUpDimensions({ y: 0, height: 0 });
    };

    useEffect(() => {
        if (selectedCompany && selectedModel) {
            setQuery(`${selectedCompany} / ${selectedModel}`);
            resetState();
        }
    }, [selectedCompany, selectedModel]);

    useEffect(() => {
        if (isEmpty(data) && query) {
            fetchDataMethod().then(data => {
                setData(data);
                setCompanies(Object.keys(data));
            });
        }
        setSelectedCompany('');
    }, [query]);

    return (
        <div className="CarFiled">
            <SearchInput inputValue={query} onChange={setQuery} autoFocus>
                {query && (
                    <DropDownList
                        data={getDataByQuery(companies, query)}
                        selected={selectedCompany}
                        onMouseEnter={handleMouseEnter}
                        onMouseLeave={handleMouseLeave}
                    />
                )}
                {selectedCompany && (
                    <PopUp
                        data={models}
                        dimensions={popUpDimensions}
                        onClick={setSelectedModel}
                        onMouseLeave={resetState}
                    />
                )}
            </SearchInput>
        </div>
    );
};

export default CarFiled;
