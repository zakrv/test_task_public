import React, { useState, useLayoutEffect } from 'react';
import { Dimensions } from '../../interfaces';

import './styles.css';

interface Props {
    data: Array<string>;
    dimensions: Dimensions;
    onMouseLeave: () => void;
    onClick: (value: string) => void;
}

const PopUp: React.FC<Props> = ({ data, dimensions, onMouseLeave, onClick }) => {
    if (!data) return null;

    const [maxHeight, setMaxHeight] = useState<number>(170); // default for 5 rows;

    const getPositionTop = () => dimensions.y - dimensions.height;

    const handleClick = (item: string) => onClick(item);

    const renderItem = (item: string, i: number) => (
        <div key={`${i}_PopUpItem`} className="PopUpItem" onClick={() => handleClick(item)}>
            {item}
        </div>
    );

    useLayoutEffect(() => {
        setMaxHeight(window.innerHeight - (dimensions.y + dimensions.height));
    }, [dimensions]);

    return (
        <div className="PopUpWrapper" onMouseLeave={onMouseLeave} style={{ top: getPositionTop() }}>
            <div className="PopUp">
                <div className="PopUpContent" style={{ maxHeight: `${maxHeight}px` }}>
                    {data.map(renderItem)}
                </div>
            </div>
        </div>
    );
};

export default PopUp;
